using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    private AudioSource spraySound;

    // Start is called before the first frame update
    void Start()
    {
        spraySound = GameObject.Find("SpraySound").GetComponent<AudioSource>();    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(gameObject);
            spraySound.Play();

            GameState.collectiblesCount += 1;
            GameState.scoreValue += 100;
        }
    }
}
