using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    private static double finalScore = 0;
    public Button playAgainBtn;

    Text score;
    // Start is called before the first frame update
    void Start()
    {
        finalScore = GameState.scoreValue;
        score = GameObject.Find("Score").GetComponent<UnityEngine.UI.Text>();
        score.text = "Score: " + finalScore;

        // Add listenr
        playAgainBtn.onClick.AddListener(PlayAgain);
    }

    private void PlayAgain()
    {
        GameState.Reset();
        SceneManager.LoadScene("Scenes/Tile");
    }
}
