using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontMovementScript : MonoBehaviour
{
    public static readonly int initialSpeed = 100;
    private static int movingSpeed = initialSpeed;
    private readonly int MAX_SPEED = 425;
    private int previousIncrement = 0;

    void Start()
    {

    }

    void Update()
    {
        transform.position += Vector3.forward * Time.deltaTime * movingSpeed;
        int currIncrement = (int)GameState.scoreValue / 500;
        bool shouldIncrement = currIncrement > previousIncrement & GameState.scoreValue > 0 & movingSpeed < MAX_SPEED;
        if (shouldIncrement)
        {
            IncreaseSpeed();
            previousIncrement = currIncrement;
        }
    }

    public static void StopMovement()
    {
        movingSpeed = 0;
    }

    public static void StartMovement(int speed)
    {
        movingSpeed = speed;
    }

    private void IncreaseSpeed()
    {
        if (movingSpeed < MAX_SPEED)
        {
            movingSpeed += 25;
            GameState.UpdateMultipler();
        }
    }
}
