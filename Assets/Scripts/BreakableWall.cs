using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BreakableWall : MonoBehaviour
{
    public InputActionReference triggerGrafitti = null;
    private GameObject player;

    private void Awake()
    {
        triggerGrafitti.action.started += StartGrafitti;
    }

    private void OnDestroy()
    {
        triggerGrafitti.action.started -= StartGrafitti;
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if ((Vector3.Distance(player.transform.position, gameObject.transform.position) <= 100) & !GameState.enableAction)
        {
            GameState.enableAction = true;
            triggerGrafitti.action.started += StartGrafitti;
        }
    }

    private void StartGrafitti(InputAction.CallbackContext context)
    {

        if (GameState.enableAction)
        {
            bool isActive = !gameObject.activeSelf;
            gameObject.SetActive(isActive);
            GameState.enableAction = false;

            triggerGrafitti.action.started -= StartGrafitti;
        }
    }
}
